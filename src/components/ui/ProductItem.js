import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TouchableNativeFeedback,
  Platform,
} from 'react-native';
import React from 'react';
import CustomButton from './CustomButton';

const ProductItem = ({item, onDetailScreen, onAddToCart}) => {
  let TouchComponent = TouchableOpacity;

  if (Platform.OS === 'android' && Platform.Version >= 21) {
    TouchComponent = TouchableNativeFeedback;
  }
  return (
    <View style={styles.card}>
      <View style={styles.touchable}>
        <TouchComponent onPress={onDetailScreen} useForeground>
          <View>
            <View style={styles.imageContainer}>
              <Image source={{uri: item.imageUrl}} style={styles.image} />
            </View>
            <View style={styles.productInfoContainer}>
              <Text style={styles.title} numberOfLines={2}>
                {item.title}
              </Text>
              <Text style={styles.price}>${item.price.toFixed(2)}</Text>
            </View>
            <View style={styles.buttonContainer}>
              <CustomButton
                title="Details"
                color="#212121"
                onPress={onDetailScreen}
              />
              <CustomButton
                title="Cart"
                bgColor="#212121"
                onPress={onAddToCart}
              />
            </View>
          </View>
        </TouchComponent>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    margin: 20,
    shadowColor: '#121212',
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 8,
    shadowOpacity: 0.26,
    backgroundColor: '#fff',
    elevation: 5,
    borderRadius: 10,
    height: 360,
  },
  touchable: {
    borderRadius: 10,
    overflow: 'hidden',
  },
  imageContainer: {
    width: '100%',
    height: '60%',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    overflow: 'hidden',
    padding: 4,
  },
  image: {
    height: '100%',
    width: '100%',
    resizeMode: 'contain',
  },
  productInfoContainer: {
    height: '20%',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    marginVertical: 4,
    marginHorizontal: 10,
    fontSize: 18,
    color: '#121212',
  },
  price: {
    fontSize: 14,
    color: '#999',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: '20%',
    paddingHorizontal: 20,
  },
});

export default ProductItem;
