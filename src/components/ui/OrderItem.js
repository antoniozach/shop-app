import {StyleSheet, Text, View, ScrollView, Pressable} from 'react-native';
import React from 'react';
import FontAwesome from 'react-native-vector-icons/FontAwesome5';
import {useDispatch} from 'react-redux';
import {removeFromCart} from '../../store/reducers/cartSlice';

const OrderItem = ({item}) => {
  const dispatch = useDispatch();

  const removeItemFromCartHandler = product => {
    dispatch(removeFromCart(product));
  };
  return (
    <View style={styles.orderContainer}>
      <View style={{flexDirection: 'row'}}>
        <Pressable onPress={() => removeItemFromCartHandler(item)}>
          <FontAwesome name="trash-alt" size={20} color={'red'} />
        </Pressable>
        <Text style={styles.itemTitle}>{item.title}</Text>
      </View>
      <Text style={styles.itemPrice}>${item.price.toFixed(2)}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  orderContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 4,
    paddingHorizontal: 8,
    marginBottom: 10,
    marginHorizontal: 20,
    backgroundColor: '#fff',
    borderRadius: 4,
    shadowColor: '#121212',
    shadowOpacity: 0.26,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 6,
    elevation: 5,
  },
  itemPrice: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#999',
  },
  itemTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#121212',
    marginLeft: 10,
  },
});

export default OrderItem;
