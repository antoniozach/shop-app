import {StyleSheet, Text, View, Pressable} from 'react-native';
import React from 'react';

const CustomButton = ({onPress, title, size, bgColor, color}) => {
  return (
    <Pressable
      style={{
        ...styles.buttonContainer,
        backgroundColor: bgColor
          ? bgColor
          : styles.buttonContainer.backgroundColor,
      }}
      onPress={onPress}>
      <Text
        style={{
          ...styles.buttonText,
          color: color ? color : styles.buttonText.color,
        }}>
        {title}
      </Text>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  buttonContainer: {
    elevation: 4,
    backgroundColor: '#ccc',
    borderRadius: 4,
    paddingVertical: 8,
    paddingHorizontal: 16,
  },
  buttonText: {
    fontSize: 16,
    color: '#fff',
    fontWeight: 'bold',
    alignSelf: 'center',
    textTransform: 'uppercase',
  },
});

export default CustomButton;
