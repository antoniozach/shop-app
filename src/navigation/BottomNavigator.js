import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import ProductsScreen from '../screens/shop/ProductsScreen';
import CartScreen from '../screens/shop/CartScreen';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const Tab = createBottomTabNavigator();
const BottomNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarActiveTintColor: '#121212',
        tabBarInactiveTintColor: '#999',
        tabBarLabelStyle: {
          fontSize: 14,
        },
      })}>
      <Tab.Screen
        name="AllProducts"
        component={ProductsScreen}
        options={{
          headerTitle: 'All Products',
          tabBarLabel: 'Products',
          tabBarIcon: ({focused, color, size}) => {
            let iconName;
            iconName = 'tshirt';
            return <FontAwesome5 name={iconName} size={20} color={color} />;
          },
        }}
      />
      <Tab.Screen
        name="Cart"
        component={CartScreen}
        options={{
          tabBarLabel: 'Cart',
          tabBarIcon: ({focused, color, size}) => {
            let iconName;
            iconName = 'shopping-cart';
            return <FontAwesome5 name={iconName} size={20} color={color} />;
          },
        }}
      />
    </Tab.Navigator>
  );
};

const styles = StyleSheet.create({});

export default BottomNavigator;
