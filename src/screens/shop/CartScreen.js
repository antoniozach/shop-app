import {StyleSheet, Text, View, FlatList} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';
import OrderItem from '../../components/ui/OrderItem';
import CustomButton from '../../components/ui/CustomButton';

const CartScreen = () => {
  const cartItems = useSelector(state => state.cartState.cartItems);
  const totalCartAmount = useSelector(state => state.cartState.totalCartAmount);
  return (
    <View style={{marginVertical: 20}}>
      <View style={styles.cartInfo}>
        <Text style={styles.cartTotal}>${totalCartAmount.toFixed(2)}</Text>
        <CustomButton
          title="Buy"
          bgColor={'#121212'}
          color={'#fff'}
          onPress={() => {}}
        />
      </View>
      <FlatList
        data={cartItems}
        renderItem={({item}) => <OrderItem item={item} />}
        keyExtractor={item => Math.random() * item.id}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  cartInfo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 8,
    paddingHorizontal: 12,
    backgroundColor: '#fff',
    margin: 20,
    marginTop: 0,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 4},
    shadowOpacity: 0.26,
    shadowRadius: 8,
    elevation: 5,
  },
  cartTotal: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#121212',
  },
});

export default CartScreen;
