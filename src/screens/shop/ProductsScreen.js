import {StyleSheet, Text, View, FlatList, Image} from 'react-native';
import React from 'react';

import {useSelector, useDispatch} from 'react-redux';
import ProductItem from '../../components/ui/ProductItem';
import {addToCart, setCartAmount} from '../../store/reducers/cartSlice';

const ProductsScreen = props => {
  const products = useSelector(state => state.productState.allProducts);
  const dispatch = useDispatch();
  const items = useSelector(state => state.cartState.cartItems);

  const addToCartHandler = product => {
    dispatch(addToCart([...items, product]));
    dispatch(setCartAmount(product.price));
  };
  return (
    <View style={styles.screen}>
      <FlatList
        data={products}
        renderItem={({item}) => (
          <ProductItem
            item={item}
            onDetailScreen={() => {
              props.navigation.navigate('ProductDetails', {
                productId: item.id,
                productTitle: item.title,
              });
            }}
            onAddToCart={() => addToCartHandler(item)}
          />
        )}
        keyExtractor={item => item.id}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    paddingVertical: 10,
  },
});

export default ProductsScreen;
