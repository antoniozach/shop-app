import {StyleSheet, Text, View, ScrollView, Image} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';
import CustomButton from '../../components/ui/CustomButton';

const ProductDetailsScreen = ({navigation, route}) => {
  const productId = route.params.productId;
  const singleProduct = useSelector(state =>
    state.productState.allProducts.find(product => productId === product.id),
  );

  return (
    <ScrollView style={{paddingVertical: 10}}>
      <View style={styles.imageContainer}>
        <Image source={{uri: singleProduct.imageUrl}} style={styles.image} />
      </View>
      <View style={styles.productInfo}>
        <Text style={styles.title}>{singleProduct.title}</Text>
        <View style={styles.buyActions}>
          <Text style={styles.price}>${singleProduct.price.toFixed(2)}</Text>
          <CustomButton title="To Cart" bgColor="#212121" onPress={() => {}} />
        </View>
        <View style={styles.descriptionContainer}>
          <Text style={styles.description}>{singleProduct.description}</Text>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  imageContainer: {
    height: 350,
    width: '100%',
    backgroundColor: '#fff',
    padding: 8,
  },
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
  },
  productInfo: {
    padding: 20,
  },
  buyActions: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#121212',
  },
  price: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#999',
  },
  descriptionContainer: {
    paddingVertical: 20,
  },
  description: {
    fontSize: 16,
    color: '#121212',
  },
});

export default ProductDetailsScreen;
