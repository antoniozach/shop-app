import {createSlice} from '@reduxjs/toolkit';
import PRODUCTS from '../../data/dummy-data';

const initialState = {
  allProducts: PRODUCTS,
  userProducts: PRODUCTS.filter(product => product.ownerId === 'u1'),
};

export const productsSlice = createSlice({
  name: 'productsState',
  initialState,
  reducers: {
    setAllProducts: (state, action) => {
      return state;
    },
  },
});

// Action creators are generated for each case reducer function
export const {setAllProducts} = productsSlice.actions;

export default productsSlice.reducer;
