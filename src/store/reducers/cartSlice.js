import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  cartItems: [],
  totalCartAmount: 0,
};

export const cartSlice = createSlice({
  name: 'cartState',
  initialState,
  reducers: {
    addToCart: (state, action) => {
      state.cartItems = action.payload;
    },
    setCartAmount: (state, action) => {
      state.totalCartAmount += action.payload;
    },
    removeFromCart: (state, action) => {
      state.cartItems = state.cartItems.filter(
        item => item.id !== action.payload.id,
      );
      state.totalCartAmount -= action.payload.price;
    },
  },
});

export const {addToCart, setCartAmount, removeFromCart} = cartSlice.actions;

export default cartSlice.reducer;
