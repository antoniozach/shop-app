import {configureStore} from '@reduxjs/toolkit';
import productsReducer from './reducers/productsSlice';
import cartReducer from './reducers/cartSlice';

const store = configureStore({
  reducer: {
    productState: productsReducer,
    cartState: cartReducer,
  },
});

export default store;
